#pragma once
#include <cstdlib>
#include <cinttypes>

template<typename ALGO_HASH>
class chash
{
public:
	using value_t = typename ALGO_HASH::value_t;
	template<typename T>
	inline auto operator()(T* data, size_t length) {
		return value = ALGO_HASH::transform((const void*)data, length, value);
	}
public:
	value_t value{ ALGO_HASH::init_value };
};
