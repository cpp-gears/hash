#pragma once
#include "chash.h"

namespace hash {
	class ccrc64
	{
	public:
        using value_t = uint64_t;
		static inline const value_t init_value{ 0 };
		static value_t transform(const void* data, size_t length, const value_t& init = init_value);
	};
}
